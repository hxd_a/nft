package com.cqupt.art.service;

import com.cqupt.art.entity.UserToken;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huangxudong
 * @since 2022-11-25
 */
public interface UserTokenService extends IService<UserToken> {

}
