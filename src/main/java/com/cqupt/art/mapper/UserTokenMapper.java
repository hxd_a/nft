package com.cqupt.art.mapper;

import com.cqupt.art.entity.UserToken;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huangxudong
 * @since 2022-11-25
 */
public interface UserTokenMapper extends BaseMapper<UserToken> {

}
