package com.cqupt.art.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author huangxudong
 * @since 2022-11-07
 */
@RestController
@RequestMapping("/pm-nft-info")
public class NftInfoController {

}
