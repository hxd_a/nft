package com.cqupt.art.service.impl;

import com.cqupt.art.entity.UserToken;
import com.cqupt.art.mapper.UserTokenMapper;
import com.cqupt.art.service.UserTokenService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huangxudong
 * @since 2022-11-25
 */
@Service
public class UserTokenServiceImpl extends ServiceImpl<UserTokenMapper, UserToken> implements UserTokenService {

}
